#!/bin/bash

# Check if the script was launched with sudo
if [ $USER != "root" ];
then
    echo '[*] Please Launch This With sudo !'
    exit 0
fi


# All vars we will need
## Miscellaneous
DATE=$(date '+%Y.%m.%d-%H.%M.%S')
ARCHIVE_NAME='stack_mattermost_backup'.$DATE
BACKUP_DIRECTORY='backup'
TEMP_DIRECTORY='tmp'
MAIN_DIRECTORY='/home/simplon'
BACKUP_FINAL_DESTINATION='simplon@52.47.162.95'
SSHKEY_PATH='/home/simplon/.ssh/id_rsa2'

## DataBase
DB_NAME='mmdb'
POSTGRE_USER='mmuser'
POSTGRE_PASSWORD='simplon'
SCHEMA_DB_FILE_NAME='db_schema'
DATA_DB_FILE_NAME='db_data'

## Software
SOFTWARE_ARRAY=('postgresql' 'nginx' 'letsencrypt' 'grafana' 'influxdb' 'telegraf')
MATTERMOST_PATH='/opt/mattermost'


echo '[*] Create Backup Folder If It Does Not Exist'
# Check if backup dir exits, if not -> create it
if [ ! -d "$MAIN_DIRECTORY/$BACKUP_DIRECTORY" ]; then
  mkdir $MAIN_DIRECTORY/$BACKUP_DIRECTORY
fi

# Check if backup/tmp dir exits, if not -> create it
if [ ! -d "$MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY" ]; then
  mkdir $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY
fi


# Backup DataBase
echo '[*] Backup DataBase Schema And Data'
## Backup db schema
pg_dump --username=$POSTGRE_USER --schema-only $DB_NAME > $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY/$SCHEMA_DB_FILE_NAME
## Backup db data
pg_dump --username=$POSTGRE_USER --data-only --disable-triggers $DB_NAME > $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY/$DATA_DB_FILE_NAME


echo '[*] Get All Software'"'"'s Folders Path'
# Get all software folder path
for ((i=0; i < ${#SOFTWARE_ARRAY[@]}; i++))
do
  TEMP_PATH_ARRAY=( $( whereis ${SOFTWARE_ARRAY[$i]}) )
  ## Get the first data in TEMP_PATH_ARRAY out
  IFS=' ' read -ra TEMP_ARR <<< ${TEMP_PATH_ARRAY[*]:1:${#TEMP_PATH_ARRAY[@]}}

  ## Backup software's folder
  for path in ${TEMP_ARR[*]}
  do
    printf '.'
    mkdir -p $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY$path
    cp -r $path $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY$path
  done

done

printf '.'
mkdir -p $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY$MATTERMOST_PATH
cp -r $MATTERMOST_PATH/* $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY$MATTERMOST_PATH
echo ''


# Create an archive with all of that
echo '[*] Create Archive'
cd $MAIN_DIRECTORY/backup/tmp/
tar -cf $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$ARCHIVE_NAME.tar *


# Delete temp folder
echo '[*] Delete Temp Folder'
cd ..
rm -r $TEMP_DIRECTORY


# Send archive to another server
echo "[*] Send Archive To Another Server With SSH"
scp -i "$SSHKEY_PATH" "$MAIN_DIRECTORY/$BACKUP_DIRECTORY/$ARCHIVE_NAME.tar"  "$BACKUP_FINAL_DESTINATION:/home/simplon/"


# Send message to discord (Webhook)
echo "[*] Send Notification On Discord"
url="https://discordapp.com/api/webhooks/466622041940819968/ChdDPfDUj9ABIK03GP_Agr0GPDvppDZiP3oZto02IFOb9AVdCKOcA95cz7l9pW2Of19A"
curl -H "Content-Type: application/json" -X POST -d '{"username": "Backup", "content": "Stack Mattermost Backup Done Well !"}' $url
