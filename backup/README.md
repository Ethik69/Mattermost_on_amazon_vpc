# This is how we do a backup :)

### We check if the script is launched sudo mode : 

```
if [ $USER != "root" ];
then
    echo '[*] Please Launch This With sudo !'
    exit 0
fi
```
### We declare the variables : 

1. Global setup

```
DATE=$(date '+%Y.%m.%d-%H.%M.%S')
ARCHIVE_NAME='stack_mattermost_backup'.$DATE
BACKUP_DIRECTORY='backup'
TEMP_DIRECTORY='tmp'
MAIN_DIRECTORY='/home/simplon'
BACKUP_FINAL_DESTINATION='simplon@52.47.162.95'
SSHKEY_PATH='/home/simplon/.ssh/id_rsa2'

```

2. We scope to the database we want to back up : 

```
DB_NAME='mmdb'
POSTGRE_USER= <youruser>
POSTGRE_PASSWORD= <yourpassword>
SCHEMA_DB_FILE_NAME='db_schema'
DATA_DB_FILE_NAME='db_data'

```

3. List of the softwares

```
SOFTWARE_ARRAY=('postgresql' 'nginx' 'letsencrypt' 'grafana' 'influxdb' 'telegraf')
MATTERMOST_PATH='/opt/mattermost'

```

### We check if the backup directory already exists, if not, we make it : 

```
echo '[*] Create Backup Folder If It Does Not Exist'

if [ ! -d "$MAIN_DIRECTORY/$BACKUP_DIRECTORY" ]; then
  mkdir $MAIN_DIRECTORY/$BACKUP_DIRECTORY
fi
```

### We check if the temporary directory already exists, if not, we make it : 

```
if [ ! -d "$MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY" ]; then
  mkdir $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY
fi
```

### Database back up to temp directory : 

1. Schema (contains informations about the DB)

```
pg_dump --username=$POSTGRE_USER --schema-only $DB_NAME > $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY/$SCHEMA_DB_FILE_NAME
```

2. Data

```
pg_dump --username=$POSTGRE_USER --data-only --disable-triggers $DB_NAME > $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY/$DATA_DB_FILE_NAME
```

### Software back up :

1. We get all the individual software paths, and we put them into an array 

```
for ((i=0; i < ${#SOFTWARE_ARRAY[@]}; i++))
do
  TEMP_PATH_ARRAY=( $( whereis ${SOFTWARE_ARRAY[$i]}) )
  ## Get the first data in TEMP_PATH_ARRAY out
  IFS=' ' read -ra TEMP_ARR <<< ${TEMP_PATH_ARRAY[*]:1:${#TEMP_PATH_ARRAY[@]}}

```
2. And for each software, we back up the folder paths

```
  for path in ${TEMP_ARR[*]}
  do
    printf '.'
    mkdir -p $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY$path
    cp -r $path $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$TEMP_DIRECTORY$path
  done

done
```

### We create and archive with all from temp directory

```
echo '[*] Create Archive'
cd $MAIN_DIRECTORY/backup/tmp/
tar -cf $MAIN_DIRECTORY/$BACKUP_DIRECTORY/$ARCHIVE_NAME.tar *
```
### We delete the temp folder

```
echo '[*] Delete Temp Folder'
cd ..
rm -r $TEMP_DIRECTORY
```

### We send the archive to another server in ssh mode

```
echo "[*] Send Archive To Another Server With SSH"
scp -i "$SSHKEY_PATH" "$MAIN_DIRECTORY/$BACKUP_DIRECTORY/$ARCHIVE_NAME.tar"  "$BACKUP_FINAL_DESTINATION:/home/simplon/"
```

### And we send a notification about the job done

```
echo "[*] Send Notification On Discord"
url="https://discordapp.com/api/webhooks/466622041940819968/ChdDPfDUj9ABIK03GP_Agr0GPDvppDZiP3oZto02IFOb9AVdCKOcA95cz7l9pW2Of19A"
curl -H "Content-Type: application/json" -X POST -d '{"username": "Backup", "content": "Stack Mattermost Backup Done Well !"}' $url
```