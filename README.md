# How to isntall Mattermost on Ubuntu (IDK of -v)
[This Is The Tuto](https://docs.mattermost.com/install/install-ubuntu-1604.html#installing-ubuntu-server-16-04-lts)

First, make sure that your server is up to date with the most recent security patches:

  ```
  sudo apt-get update
  sudo apt-get upgrade
  sudo apt-get dist-upgrade
  ```

Now that the system is up to date, you can start installing the components that make up a Mattermost system.

***

## Installing PostgreSQL DataBase server

1. Install the components:

  ```
  sudo apt-get install postgresql postgresql-contrib
  ```

2. Log in postgres account

  ```
  sudo --login --user postgres
  ```

3. Start the PostgreSQL interactive mode:

  ```
  psql
  ```

4. Create the Mattermost database.

  ```
  postgres=# CREATE DATABASE mattermost;
  ```

5. Create the Mattermost user ‘mmuser’.

  ```
  postgres=# CREATE USER mmuser WITH PASSWORD 'mmuser_password';
  ```

6. Grant the user access to the Mattermost database.

  ```
  postgres=# GRANT ALL PRIVILEGES ON DATABASE mattermost to mmuser;
  ```

7. Exit the PostgreSQL interactive mode.

  ```
  postgre=# \q
  ```

8. Log out of the postgres account.

  ```
  exit
  ```

9. Allow PostgreSQL to listen on all assigned IP Addresses.
Open ``/etc/postgresql/9.5/main/postgresql.conf`` as root in a text editor.

      * a. Find the following line:

    ```
    #listen_addresses = 'localhost'
    ```

      * b. Uncomment the line and change localhost to this:

    ```
    listen_addresses = '*'
    ```

      * c. Restart PostgreSQL for the change to take effect:

    ```
    sudo systemctl restart postgresql
    ```

10. Modify the file pg_hb.conf to allow the Mattermost server to communicate with the database.

      * a. Open ``/etc/postgresql/9.5/main/pg_hba.conf`` as root in a text editor.
      * b. Find the following line:

      ```
      local   all             all                        peer
      ```

      * c. Change peer to trust:

      ```
      local   all             all                        trust
      ```

11. Reload PostgreSQL:

  ```
  sudo systemctl reload postgresql
  ```

12. Verify that you can connect with the user mmuser.

  ```
  psql --dbname=mattermost --username=mmuser --password
  ```

***

#### Différences réalité/tuto rencontré l'ors de l'installation de PostgreSQL:
  - La version de PostgreSQL était différente 9.5 => 10 le nom du dossier l'était donc également

***

## Installing Mattermost Server

### Install Mattermost Server on a 64-bit machine.

1. Download the latest version of the Mattermost Server. In the following command, replace X.X.X with the version that you want to download:

  ```
  wget https://releases.mattermost.com/X.X.X/mattermost-X.X.X-linux-amd64.tar.gz
  ```

2. Extract the Mattermost Server files.

  ```
  tar -xvzf mattermost*.gz
  ```

3. Move the extracted file to the ``/opt`` directory.

  ```
  sudo mv mattermost /opt
  ```

4. Create the storage directory for files.

  ```
  sudo mkdir /opt/mattermost/data
  ```

`
Note:
The storage directory will contain all the files and images that your users post to Mattermost, so you need to make sure that the drive is large enough to hold the anticipated number of uploaded files and images.
`

5. Set up a system user and group called mattermost that will run this service, and set the ownership and permissions.

      * a. Create the Mattermost user and group:

    ```
    sudo useradd --system --user-group mattermost
    ```
      * b. Set the user and group mattermost as the owner of the Mattermost files:

    ```
    sudo chown -R mattermost:mattermost /opt/mattermost
    ```

      * c. Give write permissions to the mattermost group:

    ```
    sudo chmod -R g+w /opt/mattermost
    ```

6. Set up the database driver in the file ``/opt/mattermost/config/config.json``:

      * a. Set ``"DriverName"`` to ``"postgres"``
      * b. Set ``"DataSource"`` to the following value, replacing ``<mmuser-password>`` and ``<host-name-or-IP>`` with the appropriate values. Also make sure that the database name is ``mattermost`` instead of ``mattermost_test``:

    ```
    "postgres://mmuser:<mmuser-password>@<host-name-or-IP>:5432/mattermost?sslmode=disable&connect_timeout=10".
    ```

7. Test the Mattermost server to make sure everything works.

      * a. Change to the mattermost directory:

    ```
    cd /opt/mattermost
    ```

      * b. Start the Mattermost server as the user mattermost:

    ```
    sudo -u mattermost ./bin/mattermost
    ```

When the server starts, it shows some log information and the text ``Server is listening on :8065``. You can stop the server by pressing CTRL+C in the terminal window.

8. Setup Mattermost to use systemd for starting and stopping.

      * a. Create a systemd unit file:

    ```
    sudo touch /lib/systemd/system/mattermost.service
    ```

      * b. Open the unit file as root in a text editor, and copy the following lines into the file:

    ```
    [Unit]
    Description=Mattermost
    After=network.target
    After=postgresql.service
    Requires=postgresql.service

    [Service]
    Type=notify
    ExecStart=/opt/mattermost/bin/mattermost
    TimeoutStartSec=3600
    Restart=always
    RestartSec=10
    WorkingDirectory=/opt/mattermost
    User=mattermost
    Group=mattermost
    LimitNOFILE=49152

    [Install]
    WantedBy=postgresql.service
    ```

      * c. Make systemd load the new unit.

    ```
    sudo systemctl daemon-reload
    ```

      * d. Check to make sure that the unit was loaded.

    ```
    sudo systemctl status mattermost.service
    ```

    You should see an output similar to the following:

    ```
    ● mattermost.service - Mattermost
    Loaded: loaded (/lib/systemd/system/mattermost.service; disabled; vendor preset: enabled)
    Active: inactive (dead)
    ```

      * e. Start the service.

    ```
    sudo systemctl start mattermost.service
    ```

      * f. Verify that Mattermost is running.

    ```
    curl http://localhost:8065
    ```

    You should see the HTML that’s returned by the Mattermost server.

      * g. Set Mattermost to start on machine start up.

    ```
    sudo systemctl enable mattermost.service
    ```

Now that the Mattermost server is up and running, you can do some initial configuration and setup.

***

## Configuring Mattermost Server

Create the System Admin user and set up Mattermost for general use.

1. Open a browser and navigate to your Mattermost instance. ``http://server_ip:8065``.

2. Create the first team and user. The first user in the system has the ``system_admin`` role, which gives you access to the System Console.

3. Open the System Console. To open the System Console, click your username at the top of the navigation panel, and in the menu that opens, click **System Console**

4. Set the Site URL:

      * a. In the **GENERAL** section of the System Console, click Configuration.

      * b. In the **Site URL** field, set the URL that users point their browsers at.

5. Set up email notifications.

      * a. In the NOTIFICATIONS section of the System Console, click Email and make the following changes:

        - Set **Enable Email Notifications** to *true*</br>
        - Set **Notification Display Name** to *No-Reply*</br>
        - Set **Notification From Address** to *{your-domain-name}*</br>
        - Set **SMTP Server Username** to *{SMTP-username}*</br>
        - Set **SMTP Server Password** to *{SMTP-password}*</br>
        - Set **SMTP Server** to *{SMTP-server}*</br>
        - Set **SMTP Server Port** to *465*</br>
        - Set **Connection Security** to *TLS* or *STARTTLS*, depending on what the SMTP server accepts.

      * b. Click Test Connection.

      * c. After your connection is working, click Save.

6. Set up the file and image storage location.

      * a. In the FILES section of the System Console, click **Storage**.

      * b. If you store the files locally, set **File Storage System** to *Local File System*, and then either accept the default for the **Local Storage Directory** or enter a location. The location must be a directory that exists and has write permissions for the Mattermost server. It can be an absolute path or a relative path. Relative paths are relative to the ``mattermost`` directory.

      * c. If you store the files on Amazon S3, set **File Storage System** to *Amazon S3* and enter the appropriate values for your Amazon account.

      * d. Click **Save**.

7. Review the other settings in the System Console to make sure everything is as you want it.

8. Restart the Mattermost Service.

***

## Configuring TLS on Mattermost Server with let's encrypt

The easiest option is to set up TLS on the Mattermost Server, but if you expect to have more than 200 users, use a proxy for better performance. A proxy server also provides standard HTTP request logs.

#### Configure TLS on the Mattermost Server:

1. In the System Console > General > Configuration.

      * a. Change the **Listen Address** setting to *:443*.
      * b. Change the **Connection Security** setting to *TLS*.
      * c. Change the **Forward port 80 to 443** setting to *true*.

2. Activate the CAP_NET_BIND_SERVICE capability to allow Mattermost to bind to low ports.

      * a. Open a terminal window and change to the Mattermost bin directory.

    ```
    cd /opt/mattermost/bin
    ```

      * b. Run the following command:

    ```
    sudo setcap cap_net_bind_service=+ep ./mattermost
    ```

3. Install the security certificate. You can use Let’s Encrypt to automatically install and setup the certificate, or you can specify your own certificate.

  ##### To use a Let’s Encrypt certificate:

  The certificate is retrieved the first time that a client tries to connect to the Mattermost server. Certificates are retrieved for any hostname a client tries to reach the server at.

     * a. Change the **Use Let’s Encrypt** setting to *true*.

     * b. Restart the Mattermost server for these changes to take effect.

``Note:If Let’s Encrypt is enabled, forward port 80 through a firewall, with Forward80To443 config.json setting set to true to complete the Let’s Encrypt certification.``

``To use your own certificate:``

``Change the Use Let’s Encrypt setting to false.
Change the TLS Certificate File setting to the location of the certificate file.
Change the TLS Key File setting to the location of the private key file.
Restart the Mattermost server for these changes to take effect.``
